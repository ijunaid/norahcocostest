//
//  ConnectNative.m
//  Casino
//
//  Created by user  on 2015/03/30.
//
//

#include "ConnectNative.h"
#include "AppController.h"
#import <Foundation/Foundation.h>
#import <sys/utsname.h>
#include <string>

namespace native_plugin {

    std::string ConnectNative::getUniqueIdentifier(){
        std::string result( [[[[UIDevice currentDevice] identifierForVendor] UUIDString] UTF8String] );
        return result;
    }
    
    std::string ConnectNative::getDeviceModel(){
        struct utsname systemInfo;
        uname(&systemInfo);
        
        std::string result( [[NSString stringWithCString:systemInfo.machine
                                                encoding:NSUTF8StringEncoding] UTF8String] );
        return result;
    }
}
